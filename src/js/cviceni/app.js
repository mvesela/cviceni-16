/* eslint-disable consistent-return */
import domLoad from './dom-load';
import windowLoad from './window-load';
import controllerLoad from './controller-load';
import unsupportedBrowsers from '../lib/unsupported-browsers';

// The DOMContentLoaded event fires when parsing of the current page is complete. DOMContentLoaded is a great event to use to hookup UI functionality to complex web pages.
(() => {
  if (unsupportedBrowsers()) return false;

  document.addEventListener('DOMContentLoaded', domLoad, false);
  document.addEventListener('DOMContentLoaded', controllerLoad, false);
  // The load event fires when all files have finished loading from all resources, including ads and images.
})();

window.addEventListener('load', windowLoad, false);
