import {
  __mergeDeep,
  __hasClass,
  __addClass,
  __removeClass,
  __position,
  __positionTranslated,
  __remove,
  __getRealDimensions,
  __insertBefore,
  __keycodeIsPrintableCharacter,
  __toggleClass,
  __dispatchEvent,
  __isTouchEnabled,
  __isTouchOnly,
} from '../../lib/utils';
import Component from '../component';


export default class Editor extends Component {

  constructor($element) {
    super($element);

    // TODO: should extend default Component class and use this.target?
    // this.$editor is better, more readable
    this.$editor = this.target;
    this.id = this.$editor.id;
    // TODO: is DOM element before/after the editor itself.. move it to the editor?
    this.settings = JSON.parse(document.querySelector(`[data-editor-id-reference="${this.id}"]`).getAttribute('data-module-settings'));

    this.opts = __mergeDeep(Editor.defaults, this.settings);

    this.models = {
      passive: [
        'cteni',
      ],
      interactive: [
        'predznaceny', 'zvyraznovani', 'komentáře', 'psani',
      ],
      withMenu: [
        'predznaceny', 'zvyraznovani', 'komentáře',
      ],
    };

    this.totalTagId = 0;

    this.hasMenu = true;

    this.selection = null;
    this.lastSelectionSettings = {};
    this.isCollapsed = null;
    this.savedSelectionRange = null;
    this.selectionAttributes = null;
    this.dummyRangeElement = null;

    this.selectionMouseDown = false;
    this.selectionDirection = null;

    this.hasEditableContentChanged = false;
    this.lastPressedKey = null;

    this.hasToolbar = false;
    this.toolbar = {};

    this.originalContent = {};
    this.currentContent = {};

    this._handlers = [];

    this.wrapLastDeletesTimeoutClass = '';
    this.wrapLastDeletesTimeoutFunction = () => { };
    this.wrapLastAdditionsTimeoutClass = '';
    this.wrapLastAdditionsTimeoutFunction = () => { };
    this.isWrappingLastText = false;
    this.isWriting = false;

    this.$groupDeleted = null;

    this.cache = {
      body: document.body,
      window,
    };

    this.init();
  }

  init() {
    if (Editor._hasMenu(this.opts)) {
      try {
        this.$menu = this.$editor.querySelector(`.${this.opts.menuClass}`);

        // TODO: generalize .item class via defaults and opts
        this.$menuActions = this.$menu.querySelector('.menu').querySelectorAll('.menu-item');
        this.$menuClose = this.$menu.querySelector('.close');
      } catch (error) {
        this.hasMenu = false;
      }
    } else {
      this.hasMenu = false;
    }

    if (Editor._hasToolbar(this.opts)) {
      this.hasToolbar = true;
    }

    this.$canvas = this.$editor.querySelector(`.${this.opts.canvasClass}`);

    // only if the editor is INTERACTIVE
    if (this.opts.funkce !== 'cteni') {
      __addClass(this.$menu, 'hidden');

      // only if the editor is for WRITING
      if (this.opts.funkce === 'psani') {
        this.originalContent.html = this.$canvas.innerHTML;
        this.originalContent.text = this.$canvas.innerText;
        this.currentContent.html = this.$canvas.innerHTML;
        this.currentContent.text = this.$canvas.innerText;

        this.wrapLastDeletesTimeoutClass = this._generateNewTagGroupClass();
        this.wrapLastAdditionsTimeoutClass = this._generateNewTagGroupClass();
      }

      // only if the editor is with extra TOOLBAR
      if (this.hasToolbar) {
        this.toolbar.$element = this.$editor.querySelector('.editor-toolbar');
        this.toolbar.$btnReset = this.toolbar.$element.querySelector('.editor-toolbar-action-reset');
      }

      this.registerEditableHandlers();
    }

    // only if the editor is with PREHIGHLIGHTED text
    if (this.opts.funkce === 'predznaceny') {
      this.initPreHighlightedTextOptions();
    }

  }

  _generateNewTagId() {
    this.totalTagId = this.totalTagId + 1;
    return `${this.id}-${this.totalTagId}`;
  }

  _generateNewTagGroupClass() {
    this.totalTagId = this.totalTagId + 1;
    return `group-${this.id}-${this.totalTagId}`;
  }

  // add all eventListeners
  // TODO: separate special case from universal ones?
  registerEditableHandlers() {

    this.$canvas.addEventListener('mousedown', this.handlerFocus.bind(this), false);
    this.$canvas.addEventListener('touchstart', this.handlerFocus.bind(this), false);
    this.$canvas.addEventListener('focusout ', this.handlerFocus.bind(this), false);

    // Necessary when mouseup is outside the $canvas element
    // therefore we have to capture the event outside the element through body element
    this.cache.body.addEventListener('mouseup', this.handlerFocus.bind(this), false);
    this.cache.body.addEventListener('touchend', this.handlerFocus.bind(this), false);

    if (
      __isTouchOnly()
      || (window.innerWidth <= 1024 && __isTouchEnabled())
    ) {
      this.$canvas.addEventListener('contextmenu', (event) => {
        event.preventDefault();
        event.stopPropagation();
      });
    }

    if (this.opts.funkce === 'psani') {

      this.$canvas.addEventListener('keydown', (e) => {

        const selection = window.getSelection();
        const keyCode = e.which || e.keyCode;
        let keyName = e.key;

        // show OK button or feedback
        this.showFeedback();

        // start DELETE
        if (keyName === 'Backspace' || keyName === 'Delete') {
          if (selection.rangeCount) {
            window.clearTimeout(this.wrapLastDeletesTimeoutFunction);

            const range = selection.getRangeAt(0);
            const cloneRange = range.cloneRange();

            if (this.lastSelectionSettings.isCollapsed) {

              try {
                cloneRange.setStart(range.startContainer, range.startOffset - 1);
              } catch (e) {
                // backspace was pressed at the beginning of the node
                cloneRange.setStart(range.startContainer, range.startOffset);
                cloneRange.collapse(true);
              }

            } else {
              cloneRange.setStart(range.startContainer, range.startOffset);

            }

            cloneRange.setEnd(range.endContainer, range.endOffset);

            // do nothing when it's space
            const addedFragment = cloneRange.cloneContents();
            const addedFragmentString = addedFragment.textContent.trim();

            if ((addedFragmentString !== '' && addedFragmentString !== '&nbsp;') || this.isWrappingLastText) {

              const dummyElementInner = document.createElement('span');
              dummyElementInner.className = `user-removed-text-inner ${this.wrapLastDeletesTimeoutClass}`;

              cloneRange.surroundContents(dummyElementInner);

              selection.removeAllRanges();
              selection.addRange(range);

              this.wrapLastDeletesTimeoutFunction = setTimeout(() => {

                this.isWrappingLastText = false;

                // wrap all previous deletes to one element
                const dummyElement = document.createElement('span');
                dummyElement.contentEditable = false;
                dummyElement.className = 'user-removed-text';

                const newGeneratedId = this._generateNewTagId();
                dummyElement.id = newGeneratedId;

                const $elements = document.querySelectorAll(`.${this.wrapLastDeletesTimeoutClass}`);

                __insertBefore(dummyElement, $elements[0]);

                $elements.forEach((el, i) => {
                  dummyElement.appendChild(el);
                });


                // create close button
                const $btnClose = document.createElement('span');
                $btnClose.className = this.opts.closeBtnClass;

                const clickHandler = (e) => {
                  const $nodeElement = e.currentTarget.parentNode;
                  this._removeNodeKeepTextContent($nodeElement);
                  e.stopPropagation();
                };

                $btnClose.addEventListener('click', clickHandler.bind(this));

                document.getElementById(newGeneratedId).appendChild($btnClose);

                this.wrapLastDeletesTimeout = true;
                this.wrapLastDeletesTimeoutClass = this._generateNewTagGroupClass();

              }, 700);

              this.isWrappingLastText = true;

              e.preventDefault();

            } else {

              if (!this.isWrappingLastText) {
                // we're removing spaces, do nothing use browser's default behaviour, unless the wrapping is going on
                return;
              }
              e.preventDefault();
            }
          }
        }
        // end DELETE

        // start WRITING
        else if (__keycodeIsPrintableCharacter(keyCode)) {

          // if nothing is selected (selection is empty)
          if (this.lastSelectionSettings.isCollapsed) {

            // if "space" was written
            if (keyCode === 32) {
              if (!this.isWrappingLastText) {
                // we're adding spaces, do nothing use browser's default behaviour, unless the wrapping is going on
                return;
              }
              // fill a space as the keyname for the spacebar
              keyName = ' ';
            }

            // The Selection.rangeCount read-only property returns the number of ranges in the selection.
            if (selection.rangeCount) {

              window.clearTimeout(this.wrapLastAdditionsTimeoutFunction);

              const range = selection.getRangeAt(0);
              const cloneRange = range.cloneRange();

              cloneRange.setStart(range.startContainer, range.startOffset);
              cloneRange.setEnd(range.endContainer, range.endOffset);

              const textToAddElement = document.createElement('span');
              textToAddElement.contentEditable = false;
              textToAddElement.className = `user-inserted-text-inner ${this.wrapLastAdditionsTimeoutClass}`;
              textToAddElement.innerHTML = keyName;

              cloneRange.insertNode(textToAddElement);

              // move the caret after the inserted character
              range.setStartAfter(textToAddElement);

              selection.removeAllRanges();
              selection.addRange(range);

              this.wrapLastAdditionsTimeoutFunction = setTimeout(() => {

                this.isWrappingLastText = false;

                // wrap all previous deletes to one element
                const dummyElement = document.createElement('span');
                dummyElement.contentEditable = false;
                dummyElement.className = 'user-inserted-text';

                const newGeneratedId = this._generateNewTagId();
                dummyElement.id = newGeneratedId;

                const $elements = document.querySelectorAll(`.${this.wrapLastAdditionsTimeoutClass}`);

                __insertBefore(dummyElement, $elements[0]);

                $elements.forEach((el, i) => {
                  dummyElement.appendChild(el);
                });


                // create close button
                const $btnClose = document.createElement('span');
                $btnClose.className = this.opts.closeBtnClass;

                const clickHandler = (e) => {
                  const $nodeElement = e.currentTarget.parentNode;
                  __remove($nodeElement);
                  e.stopPropagation();
                };

                $btnClose.addEventListener('click', clickHandler.bind(this));
                document.getElementById(newGeneratedId).appendChild($btnClose);

                this.wrapLastAdditionsTimeoutClass = this._generateNewTagGroupClass();

              }, 700);

              this.isWrappingLastText = true;

            }


            if (this.hasEditableContentChanged === false) {
              this.hasEditableContentChanged = true;
            }

            e.preventDefault();
          }

          // writing over existing text is selected
          else {
            window.clearTimeout(this.wrapLastAdditionsTimeoutFunction);

            const range = selection.getRangeAt(0);
            const cloneRange = range.cloneRange();

            cloneRange.setStart(range.startContainer, range.startOffset);
            cloneRange.setEnd(range.endContainer, range.endOffset);

            // 1. handle deleting the text that was selected when writing started
            if (this.isWriting === false && selection.toString().length > 0) {
              // add deleted text node only for the first time and only if there is some selected text
              this.$groupDeleted = document.createElement('span');
              this.$groupDeleted.contentEditable = false;
              this.$groupDeleted.className = 'user-removed-text';

              const groupDeletedId = this._generateNewTagId();
              this.$groupDeleted.id = groupDeletedId;

              const $groupInnerDeleted = document.createElement('span');
              $groupInnerDeleted.className = 'user-removed-text-inner';

              this.$groupDeleted.appendChild($groupInnerDeleted);

              cloneRange.surroundContents(this.$groupDeleted);

              // create close button
              const $btnClose = document.createElement('span');
              $btnClose.className = this.opts.closeBtnClass;

              const clickHandler = (e) => {
                const $nodeElement = e.currentTarget.parentNode;
                this._removeNodeKeepTextContent($nodeElement);
                e.stopPropagation();
              };

              $btnClose.addEventListener('click', clickHandler.bind(this));
              this.$groupDeleted.appendChild($btnClose);
            }


            // 2. adding new text
            this.isWriting = true;

            const $groupInnerAdded = document.createElement('span');
            $groupInnerAdded.contentEditable = false;
            $groupInnerAdded.className = `user-inserted-text-inner ${this.wrapLastAdditionsTimeoutClass}`;

            // add a space so there is a space between the added and removed text
            $groupInnerAdded.innerHTML = `${keyName}`;

            // TODO: error after deleted selection
            __insertBefore($groupInnerAdded, this.$groupDeleted);
            range.setStartAfter($groupInnerAdded);

            selection.removeAllRanges();
            selection.addRange(range);

            this.wrapLastAdditionsTimeoutFunction = setTimeout(() => {
              this.isWriting = false;

              // wrap all previous deletes to one element
              const $groupAdded = document.createElement('span');
              $groupAdded.contentEditable = false;
              $groupAdded.className = 'user-inserted-text';

              const newGeneratedId = this._generateNewTagId();
              $groupAdded.id = newGeneratedId;

              const $elements = document.querySelectorAll(`.${this.wrapLastAdditionsTimeoutClass}`);

              __insertBefore($groupAdded, $elements[0]);

              $elements.forEach((el, i) => {
                $groupAdded.appendChild(el);
              });

              const $btnClose = document.createElement('span');
              $btnClose.className = this.opts.closeBtnClass;

              const clickHandler = (e) => {
                const $nodeElement = e.currentTarget.parentNode;
                __remove($nodeElement);
                e.stopPropagation();
              };

              $btnClose.addEventListener('click', clickHandler.bind(this));
              document.getElementById(newGeneratedId).appendChild($btnClose);

              this.wrapLastAdditionsTimeoutClass = this._generateNewTagGroupClass();

            }, 700);

            e.preventDefault();
          }
        }
        // end WRITING
      });
    }


    if (this.hasMenu) {
      this.registerMenuHandler();
    }

    if (this.hasToolbar) {
      this.toolbar.$btnReset.addEventListener('click', () => {

        const isConfirmed = confirm('Opravdu chcete vrátit text do původní podoby? Vaše změny se tímto ztratí.');

        if (isConfirmed) {
          this.$canvas.innerHTML = this.originalContent.html;
        }

      });
    }

  }

  registerMenuHandler() {
    for (let i = 0, len = this.$menuActions.length; i < len; i += 1) {
      this.$menuActions[i].addEventListener('click', this.handlerMenuActions.bind(this), false);
    }

    this.$menuClose.addEventListener('click', this.handlerMenuClose.bind(this), false);
  }

  initPreHighlightedTextOptions() {
    // Example of data from JSON:
    //
    // "predznaceni": [
    //     {
    //       "vyraz": "Obracíme se na vás",
    //       "data": {
    //         "osoba": "my",
    //         "slovesnyZpusob": "oznamovací"
    //       }
    //     }
    //   ]
    // }
    //
    // In PUG, object for every item is injected into `data-text-option` so this JS has access to it

    const dataAttr = 'data-text-option';

    // loop over updated DOM via DOM manipulation
    this.$textOptionsElements = this.$editor.querySelectorAll(`[${dataAttr}]`);
    this.$textOptionsElements.forEach(($textOption) => {

      $textOption.addEventListener('click', () => {
        __toggleClass($textOption, this.opts.textOptions.selectedClass);
        const idForSend = this.id.replace('editor-', '');

        if (__hasClass($textOption, this.opts.textOptions.selectedClass)) {

          // dispatch event that editor has changed
          const tagData = JSON.parse($textOption.getAttribute('data-text-option'));
          tagData.tagName = 'vyznacena-pasaz';

          __dispatchEvent(document, 'editor.change', {}, {
            text: {
              id: idForSend,
              task: 'add',
              tagData,
              value: $textOption.cloneNode(true),
            },
          });
        } else {
          // dispatch event that editor has changed
          __dispatchEvent(document, 'editor.change', {}, {
            text: {
              id: idForSend,
              task: 'remove',
              tagData: { tagName: 'vyznacena-pasaz' },
              value: $textOption,
            },
          });
        }

        // show OK button or feedback
        this.showFeedback();
      });

    });

  }

  toggleMenu(clickClose = false) {

    if (clickClose === true) {
      __addClass(this.$menu, 'hidden');
      return;
    }

    if (this.selectionMouseDown === true) {
      __addClass(this.$menu, 'hidden');
      return;
    }

    if (__hasClass(this.$menu, 'hidden') && this.selectionMouseDown === false) {
      this._setmenuDimensions();
      __removeClass(this.$menu, 'hidden');
    }

  }

  _setmenuDimensions() {
    const menuDimensions = __getRealDimensions(this.$menu);
    const parentTranslatedPosition = __positionTranslated(this.$editor);
    const parentDimensions = __position(this.$editor);

    // let additionalSize = ( this.selectionDirection === "right" ) ?

    const isOverflowAt = {
      right: (this.selectionAttributes.left + this.selectionAttributes.width + menuDimensions.width + this.opts.menuMargin.x) > (parentTranslatedPosition.left + parentDimensions.width),
      top: (this.selectionAttributes.top - menuDimensions.height - this.opts.menuMargin.y) < (parentTranslatedPosition.top),
      bottom: (this.selectionAttributes.top + this.selectionAttributes.height + menuDimensions.height + this.opts.menuMargin.y) > (parentTranslatedPosition.top + parentDimensions.height),
    };

    let leftDimensions;
    let topDimensions;

    if (isOverflowAt.right) {

      leftDimensions = this.selectionAttributes.left - menuDimensions.width - this.opts.menuMargin.x;

    } else {

      leftDimensions = this.selectionAttributes.left + this.selectionAttributes.width + this.opts.menuMargin.x;

    }

    if (isOverflowAt.top) {

      topDimensions = this.selectionAttributes.top + menuDimensions.height + this.opts.menuMargin.y;

    } else {

      topDimensions = this.selectionAttributes.top - menuDimensions.height - this.opts.menuMargin.y;

    }

    this.$menu.style.left = `${leftDimensions}px`;
    this.$menu.style.top = `${topDimensions}px`;

  }

  handlerMenuClose(e) {

    this.toggleMenu(true);

    e.preventDefault();

  }

  handlerMenuActions(e) {

    const $el = e.currentTarget;
    const command = $el.getAttribute('data-command');
    const commandName = $el.getAttribute('data-command-name');

    /* :TODO: Cache the options for given element to avoid JSON parsing each time */
    const options = JSON.parse($el.getAttribute('data-command-options'));

    this.setSelection(command, commandName, options);

    e.preventDefault();

  }

  handlerFocus(e) {
    if (e.target.closest('.canvas') === null) return;

    const isMouseRightClick = e.which === 3;

    if (isMouseRightClick) { return; }

    if (__hasClass(e.target, 'node-close')) { return; }

    if (__hasClass(e.target, 'node-selected')) { return; }

    if (e.type === 'mousedown' || e.type === 'touchstart') {

      // start tracking the mouse down
      this.selectionMouseDown = true;

      if (this.hasMenu === true) {
        this.toggleMenu();
      }

    }

    if ((e.type === 'mouseup' || e.type === 'touchend') && this.selectionMouseDown) {
      e.preventDefault();
      e.stopPropagation();

      // reset saved last selection settings
      this.lastSelectionSettings = {};

      this.selection = window.getSelection();
      this.isCollapsed = this.selection.isCollapsed;
      this.lastSelectionSettings.isCollapsed = this.selection.isCollapsed;
      this.savedSelectionRange = this.selection.getRangeAt(0);

      this.selectionMouseDown = false;

      this.lastSelectionSettings.anchorOffset = this.selection.anchorOffset;
      this.lastSelectionSettings.focusOffset = this.selection.focusOffset;
      this.lastSelectionSettings.text = this.savedSelectionRange.toString();

      /* https://developer.mozilla.org/en-US/docs/Web/API/Selection/isCollapsed */
      /* Returns a Boolean indicating whether the selection's start and end points are at the same position. */
      /* display context menu only if the cursor is NOT at the same position */
      if (this.selection.isCollapsed === false && this.savedSelectionRange.toString().trim() !== '') {

        this.selectionDirection = (this.selection.anchorOffset <= this.selection.focusOffset) ? 'right' : 'left';

        if (this.hasMenu === true) {

          this._setSelectionPosition(); // :TODO: cannot be used in contentEditable editor yet

          this.toggleMenu();
        }

      }

    }

  }

  _setSelectionPosition() {

    const dummyElement = document.createElement('span');
    dummyElement.className = 'dummy-range-element';

    this.savedSelectionRange.surroundContents(dummyElement);

    const selectionPosition = __positionTranslated(dummyElement);
    const selectionDimension = __position(dummyElement);

    this.selectionAttributes = {
      left: selectionPosition.left,
      top: selectionPosition.top,
      width: selectionDimension.width,
      height: selectionDimension.height,
    };

    __removeClass(dummyElement, 'dummy-range-element');

    this._removeNodeKeepTextContent(dummyElement);

  }

  _highlightContents($domEl, newTagId) {
    $domEl.appendChild(this.savedSelectionRange.extractContents());
    this.savedSelectionRange.insertNode($domEl);

    $domEl.setAttribute('data-tag-id', newTagId);
  }

  _storeHighlightedContents($domEl, newTagId) {
    const tagName = $domEl.getAttribute('data-command-name');

    // get tag color
    const classes = $domEl.getAttribute('class');
    // const colors = classes.match(/(?<=node-text-)[a-z]*/g);
    const classesArray = classes.split(' ');
    let color = '';
    classesArray.forEach((value) => {
      if (value.includes('node-text-')) {
        color = value.replace('node-text-', '');
      }
    });

    // dispatch event that editor has changed
    const idForSend = this.id.replace('editor-', '');
    __dispatchEvent(document, 'editor.change', {}, {
      text: {
        id: idForSend,
        task: 'add',
        tagData: { tagName },
        value: $domEl.cloneNode(true),
        tagColor: color,
      },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  _removeNodeKeepTextContent(node) {
    const text = node.textContent;
    node.parentNode.insertBefore(document.createTextNode(text), node);
    __remove(node);
  }

  _createCloseBtnForElement($parentEl, opts = {}) {

    const $elClose = document.createElement('span');
    $elClose.innerHTML = '×';
    $elClose.setAttribute('class', opts.class);

    const classesToToggle = opts.toggleClass.split(' ');


    const clickHandler = (e) => {

      const $closeBtn = e.currentTarget;
      const $nodeElement = $closeBtn.parentNode;

      $closeBtn.removeEventListener('click', clickHandler);
      $nodeElement.removeChild($closeBtn);

      if (undefined !== $nodeElement.commentRef) {

        /* :TODO: temporary way to save the reference to the DOM element */
        this.$editor.removeChild($nodeElement.commentRef);
        $nodeElement.commentRef = null;

      }

      this._removeNodeKeepTextContent($nodeElement);

      const tagName = $nodeElement.getAttribute('data-command-name');

      // dispatch event that editor has changed
      const idForSend = this.id.replace('editor-', '');
      __dispatchEvent(document, 'editor.change', {}, {
        text: {
          id: idForSend,
          task: 'remove',
          tagData: { tagName },
          value: $nodeElement,
        },
      });

      e.stopPropagation();

    };

    $elClose.addEventListener('click', clickHandler.bind(this));

    $parentEl.appendChild($elClose);

  }

  _calculateCommentPosition($node) /* : Object */ {

    const nodePosition = __positionTranslated($node);

    return {
      left: `${nodePosition.left + parseInt($node.offsetWidth, 10) + this.opts.commentMargin.x}px`,
      top: `${nodePosition.top + this.opts.commentMargin.y}px`,
    };

  }

  _createCommentForNode($el, opts = {}) {

    const commentTemplate = document.getElementById('comment-template').innerHTML;

    const div = document.createElement('div');
    div.innerHTML = commentTemplate;

    const commentDiv = div.childNodes[1];

    // attach js reference to the dom element
    commentDiv.data = {};
    commentDiv.data.nodeRef = $el;

    const commentNewTextarea = commentDiv.querySelector('.comment-new-text');
    const commentDivButtonSubmit = commentDiv.querySelector('button');
    const commentCommentsContainer = commentDiv.querySelector('.comments-container');

    const calculatedCommentPosition = this._calculateCommentPosition($el);

    commentDiv.style.left = calculatedCommentPosition.left;
    commentDiv.style.top = calculatedCommentPosition.top;

    __addClass(commentDiv, opts.class);

    commentDiv.addEventListener('click', (e) => {

      if (__hasClass(e.target, 'close')) {

        __addClass(commentDiv, 'hidden');
        __removeClass($el, 'active');

      }

    });

    commentDivButtonSubmit.addEventListener('click', (e) => {

      if (commentNewTextarea.value !== '') {

        const commentItem = document.createElement('div');
        commentItem.className = 'comment-item';

        commentItem.innerText = commentNewTextarea.value;

        commentCommentsContainer.appendChild(commentItem);

        commentNewTextarea.value = '';
      }

    });

    this.$editor.append(commentDiv);

    __addClass($el, 'comment-created');

    /* :TODO: Temporary way to save the reference to the DOM element */
    $el.commentRef = commentDiv;


  }

  _addCommentHandlerForElement($el, opts = {}) {

    __addClass($el, 'has-comments');

    function clickHandler(e) {

      if (!__hasClass(e.target, 'node-close')) {

        if (!__hasClass($el, 'comment-created')) {

          this._createCommentForNode($el, opts);

        } else {

          /* recalculate comment div position */
          const calculatedCommentPosition = this._calculateCommentPosition($el);

          $el.commentRef.style.left = calculatedCommentPosition.left;
          $el.commentRef.style.top = calculatedCommentPosition.top;

          __removeClass($el.commentRef, 'hidden');

        }

        if (!__hasClass($el, 'active')) {

          __addClass($el, 'active');

        } else {

          __removeClass($el, 'active');
          __addClass($el.commentRef, 'hidden');

        }

      }

      e.stopPropagation();

    }

    $el.addEventListener('click', clickHandler.bind(this));

  }

  setSelection(command, commandName, opts = {}) {

    if (this.selection) {

      if (command === 'toggle-class') {

        const $el = document.createElement('span');
        $el.setAttribute('class', opts.toggleClass);
        $el.setAttribute('data-command-name', commandName);

        const newTagId = this._generateNewTagId();

        this._highlightContents($el, newTagId);

        if (opts.closeBtn === true) {
          this._createCloseBtnForElement($el, {
            class: 'node-close',
            toggleClass: opts.toggleClass,
          });
        }

        if (opts.comments === true) {
          this._addCommentHandlerForElement($el, {
            class: opts.commentsClass,
          });
        }

        this._storeHighlightedContents($el, newTagId);

        if (opts.showCommentsOnInit === true) {
          $el.click();
        }

      }

      // hide context menu after selecting the action#
      this.toggleMenu(true);

      // show OK button or feedback
      this.showFeedback();

    }

  }

}

Editor.defaults = {
  menuClass: 'contextual-menu-component',
  canvasClass: 'canvas',
  resetBtnClass: 'reset',
  closeBtnClass: 'node-close',
  nodeSelectedClass: 'node-selected',
  commentMargin: {
    x: 50,
    y: 0,
  },
  menuMargin: {
    x: 15,
    y: 15,
  },
  textOptions: {
    enable: false,
    animatedClass: 'active',
    selectedClass: 'selected',
    deselectedClass: 'deselected',
  },

};

Editor._hasMenu = (opts) => opts.funkce !== 'cteni' && opts.funkce !== 'psani' && (opts.menu !== false && opts.menu !== undefined);
Editor._hasToolbar = (opts) => opts.funkce === 'psani' || (opts.toolbar !== false && opts.toolbar !== undefined);
