const translations = {
  _common: {
    close: {
      cs: 'Zavřít',
      en: 'Close',
    },
  },
  editor: {
    reset: {
      cs: 'Obnovit původní text',
      en: 'Reset to original text',
    },
  },
  fakeMap: {
    switchLayers: {
      cs: 'Přepínání vrstev',
      en: 'Switch layers',
    },
  },
  feedback: {
    default: {
      cs: 'Mám hotovo!',
      en: 'Done!',
    },
    hover: {
      cs: 'Další slajd →',
      en: 'Next slide →',
    },
    text: {
      none: {
        cs: 'Napiš něco!',
        en: 'Write something!',
      },
      short: {
        cs: 'Není toho moc',
        en: 'Not much of text',
      },
      ok: {
        cs: 'Mám hotovo!',
        en: 'I am done!',
      },
    },
  },
  fullscreen: {
    enlarge: {
      cs: 'Zvětšit',
      en: 'Enlarge',
    },
  },
  help: {
    help: {
      cs: 'Nápověda',
      en: 'Help',
    },
  },
  media: {
    transcript: {
      cs: 'Přepis nahrávky',
      en: 'Transcript',
    },
  },
  navigation: {
    next: {
      cs: 'Další',
      en: 'Next',
    },
    previous: {
      cs: 'Předchozí',
      en: 'Previous',
    },
    hotkeys: {
      cs: 'Klávesové zkratky',
      en: 'Shortcuts',
    },
  },
  other: {
    loading: {
      cs: 'Načítám cvičení',
      en: 'Loading',
    },
    loaded: {
      cs: 'Hotovo',
      en: 'Ready',
    },
    unsupported: {
      sorry: {
        cs: 'Omlouváme se, ale cvičení nelze spustit.',
        en: 'Sorry, the activity cannot be started.',
      },
      update: {
        cs: 'Prosím, aktualizujte si internetový prohlížeč (<a href="https://www.mozilla.org/firefox/" target="_blank">Firefox</a>, <a href="https://www.google.com/chrome/" target="_blank">Chrome</a>, <a href="https://www.microsoft.com/windows/microsoft-edge" target="_blank">Edge</a>).',
        en: 'Please update your internet browser (<a href="https://www.mozilla.org/firefox/" target="_blank">Firefox</a>, <a href="https://www.google.com/chrome/" target="_blank">Chrome</a>, <a href="https://www.microsoft.com/windows/microsoft-edge" target="_blank">Edge</a>).',
      },
      version: {
        cs: 'Verze vašeho prohlížeče:',
        en: 'Your browser\'s version',
      },
    },
  },
  slideExport: {
    default: {
      h1: {
        cs: 'Výborně! Úspěšně jste prošli celým cvičením.',
        en: 'Great! You have completed the activity.',
      },
      h2: {
        cs: 'Teď už ho stačí jen uložit',
        en: 'Save it now!',
      },
      save: {
        cs: 'Uložit cvičení',
        en: 'Save activity',
      },
      return: {
        cs: '… nebo se vrátit zpět a vylepšit ho',
        en: '… or go back and improve it',
      },
      privacyPolicy: {
        cs: 'Klinutím na tlačítko „Uložit cvičení“ souhlasíte se zpracováním osobních údajů a poskytnutím výsledků cvičení k dalšímu výzkumu a vývoji aplikace Historylab. Informace o zpracování osobních údajů naleznete <a href="https://historylab.cz/informace-o-zpracovani-osobnich-udaju.html" target="_blank">zde</a>.',
        en: '',
      },
      logolink: {
        cs: 'Projekt vznikl díky grantu Technologické agentury ČR',
        en: 'The project was funded by the Technology Agency of the Czech Republic',
      },
    },
    done: {
      h1: {
        cs: 'Konec vyplněného cvičení od',
        en: 'The end of the completed activity from',
      },
      return: {
        cs: 'vrátit se a ještě prohlédnout',
        en: 'go back and review the activity again',
      },
    },
    userText: {
      cs: 'Napadlo vás k tématu cvičení něco dalšího?',
      en: 'Do you have any other thoughts on the topic?',
    },
  },
  slideForm: {
    default: {
      welcome: {
        cs: 'Vítejte ve cvičení',
        en: 'Welcome to the activity',
      },
      missingAnotation: {
        cs: 'Omlouváme se, pro toto cvičení ještě nemáme anotaci.',
        en: 'Sorry, we haven\'t prepared an annotation for this activity yet.',
      },
      instruction: {
        cs: 'Pokud budete chtít cvičení uložit, vyplňte následující formulář',
        en: 'If you want to save this exercise, fill in the following form',
      },
      aboutMe: {
        cs: 'Údaje o mně',
        en: 'About me',
      },
      required: {
        cs: 'povinné',
        en: 'required',
      },
      name: {
        cs: 'Mé jméno a příjmení',
        en: 'My first name and surname',
      },
      email: {
        cs: 'Můj e-mail',
        en: 'My e-mail address',
      },
      eg: {
        cs: 'např.',
        en: 'eg.',
      },
      aboutInstitution: {
        cs: 'Údaje o škole a učiteli',
        en: 'About your institution & teacher',
      },
      institution: {
        cs: 'Název a místo mé školy',
        en: 'Name and location of your school',
      },
      emailTeacher: {
        cs: 'E-mail mého učitele',
        en: 'Your teacher\'s e-mail',
      },
      mode: {
        cs: 'Způsob práce',
        en: 'Working method',
      },
      individually: {
        cs: 'samostatně',
        en: 'individually',
      },
      group: {
        cs: 've skupině',
        en: 'group',
      },
      class: {
        cs: 's celou třídou',
        en: 'class',
      },
      groupEmails: {
        cs: 'E-maily dalších členů skupiny',
        en: 'Other members of your group\'s email addresses',
      },
      groupEmailsPlaceholder: {
        cs: 'Zde napište e-mail člena skupiny',
        en: 'Write the email address of a member of your group here',
      },
      groupEmailsAdd: {
        cs: '+ Přidat dalšího člena skupiny',
        en: '+ Add another group member',
      },
      button: {
        default: {
          cs: 'Mám vyplněno',
          en: 'I\'m done with the form',
        },
        hover: {
          cs: 'Začít →',
          en: 'Start the activity →',
        },
      },
    },
    done: {
      welcome: {
        cs: 'Vítejte ve vyplněném cvičení',
        en: 'Welcome to the completed activity',
      },
      completedBy: {
        cs: 'Cvičení vyplnil',
        en: 'Completed by',
      },
      button: {
        cs: 'Prohlédnout vyplněné cvičení',
        en: 'View the completed activity',
      },
    },
  },
  svg: {
    addText: {
      cs: 'Přidat text',
      en: 'Add text',
    },
    changeColor: {
      cs: 'Změnit barvu',
      en: 'Change color',
    },
    comics: {
      narrator: {
        cs: 'Vypravěč',
        en: 'Narrator',
      },
      speech: {
        cs: 'Promluva…',
        en: 'Speech…',
      },
      thought: {
        cs: 'Myšlenka…',
        en: 'Thought…',
      },
    },
    remove: {
      cs: 'Vymazat prvek',
      en: 'Remove item',
    },
  },
  userText: {
    placeholder: {
      cs: 'Napadlo mě, že…',
      en: 'I think that…',
    },
  },
  video: {
    muted: {
      cs: 'Bez zvuku',
      en: 'Muted',
    },
  },
};

module.exports = translations;
